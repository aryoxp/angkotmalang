package ap.mobile.angkotmalang;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import ap.mobile.angkotmalang.base.TransportRoute;

/**
 * Created by Aryo on 3/25/2018.
 */

public class RouteAdapter extends RecyclerView.Adapter<RouteAdapter.ViewHolder> {

    private static final int TYPE_NORMAL = 1;
    private static final int TYPE_EMPTY = 0;

    private Context context;
    private ArrayList<TransportRoute> routeSet;
    private RouteItemClickListener routeItemClickListener;

    public RouteAdapter(Context context, ArrayList<TransportRoute> routeSet, RouteItemClickListener listener) {
        this.context = context;
        this.routeSet = routeSet;
        this.routeItemClickListener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView;
        ViewHolder vh;
        if(viewType == TYPE_NORMAL) {
            itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_route, parent, false);
            vh = new ViewHolder(itemView);
        } else {
            itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_empty, parent, false);
            vh = new EmptyViewHolder(itemView);
        }
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {

        switch(getItemViewType(position)) {
            case TYPE_NORMAL:
                TransportRoute transportRoute = routeSet.get(position);
                int cost = (transportRoute.getCost() + CDM.standardCost(context));
                holder.tvRouteOrder.setText(TextUtils.join(" - ", transportRoute.getOrder()));
                holder.tvRouteCost.setText("Rp. " + String.valueOf(cost) + "    " + Helper.degreeToMeter(transportRoute.getDistance()) + "m");
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (routeItemClickListener != null)
                            routeItemClickListener.onRouteItemClicked(routeSet.get(position));
                    }
                });
                break;
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        if(this.routeSet.size() == 0) return TYPE_EMPTY;
        else return TYPE_NORMAL;
    }

    @Override
    public int getItemCount() {
        if(this.routeSet.size() == 0) return 1;
        return this.routeSet.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvRouteOrder;
        TextView tvRouteCost;

        public ViewHolder(View itemView) {
            super(itemView);
            this.tvRouteCost = itemView.findViewById(R.id.tvRouteCost);
            this.tvRouteOrder = itemView.findViewById(R.id.tvRouteOrder);
        }
    }

    public class EmptyViewHolder extends ViewHolder {
        public EmptyViewHolder(View itemView) {
            super(itemView);
        }
    }

    public interface RouteItemClickListener {
        void onRouteItemClicked(TransportRoute transportRoute);
    }
}
