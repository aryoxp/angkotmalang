package ap.mobile.angkotmalang;

import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;

public class SettingsActivity extends AppCompatPreferenceActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getFragmentManager().beginTransaction()
                .replace(android.R.id.content, new ServiceSettingsFragment())
                .commit();
    }

    public static class ServiceSettingsFragment extends PreferenceFragment {

        @Override
        public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_service);

            bindPreferenceValueToSummary(findPreference("pref_hostname"));
            bindPreferenceValueToSummary(findPreference("pref_basepath"));
            bindPreferenceValueToSummary(findPreference("pref_distance"));
            bindPreferenceValueToSummary(findPreference("pref_cost"));
        }
    }

    private static void bindPreferenceValueToSummary(Preference preference) {
        preference.setOnPreferenceChangeListener(preferenceChangeListener);
        preferenceChangeListener.onPreferenceChange(preference,
                PreferenceManager
                        .getDefaultSharedPreferences(preference.getContext())
                        .getString(preference.getKey(), ""));
    }

    private static Preference.OnPreferenceChangeListener preferenceChangeListener = new Preference.OnPreferenceChangeListener() {
        @Override
        public boolean onPreferenceChange(Preference preference, Object newValue) {
            if(preference.getKey().equals("pref_distance"))
                preference.setSummary(newValue.toString() + "m");
            else if(preference.getKey().equals("pref_cost"))
                preference.setSummary("Rp" + newValue.toString());
            else preference.setSummary(newValue.toString());
            return true;
        }
    };

}
