package ap.mobile.angkotmalang;

import android.content.Context;
import android.preference.PreferenceManager;

/**
 * Created by Aryo on 3/25/2018.
 */

class CDM {

    public static String defaultHostname = "175.45.187.243";
    public static String defaultBasepath = "mpt-service";
    public static int defaultDistance = 500;
    public static double oneMeterInDegree = 0.00000898448;

    public static String baseUrl(Context context) {
        String hostname = PreferenceManager.getDefaultSharedPreferences(context)
                .getString("pref_hostname", CDM.defaultHostname);
        String basepath = PreferenceManager.getDefaultSharedPreferences(context)
                .getString("pref_basepath", CDM.defaultBasepath);
        Boolean useHttps = PreferenceManager.getDefaultSharedPreferences(context)
                .getBoolean("pref_https", false);
        String url = (useHttps?"https://":"http://") + hostname + "/" + basepath + "/";
        return url;
    }

    public static int standardCost(Context context) {
        int cost = Integer.valueOf(PreferenceManager.getDefaultSharedPreferences(context)
                .getString("pref_cost", "4000"));
        return cost;
    }

}
