package ap.mobile.angkotmalang.base;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by Aryo on 3/25/2018.
 */

public class Point {

    protected String id;
    protected String name;
    protected Double lat;
    protected Double lng;

    public Point(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public Point(String id, String name, Double lat, Double lng) {
        this(id, name);
        this.lat = lat;
        this.lng = lng;
    }

    public void setLatLng(Double lat, Double lng) {
        this.lat = lat;
        this.lng = lng;
    }

    public String getId() { return this.id; }
    public Double getLat() { return this.lat; }
    public Double getLng() { return this.lng; }
    public LatLng getLatLng() { return new LatLng(this.lat, this.lng); }

}
