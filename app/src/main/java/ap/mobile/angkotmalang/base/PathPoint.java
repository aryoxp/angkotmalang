package ap.mobile.angkotmalang.base;

/**
 * Created by Aryo on 3/25/2018.
 */

public class PathPoint extends Point {

    protected String direction;
    protected String color;
    protected Double distance;
    protected int price;

    public PathPoint(String id, String name, String direction, String color, Double lat, Double lng, Double distance, int price) {
        super(id, name, lat, lng);
        this.direction = direction;
        this.color = color;
        this.distance = distance;
        this.price = price;
    }

    public String getColor() {
        return color;
    }
}
