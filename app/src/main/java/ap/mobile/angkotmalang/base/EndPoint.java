package ap.mobile.angkotmalang.base;

/**
 * Created by Aryo on 3/25/2018.
 */

public class EndPoint extends Point {

    public String direction;
    public Double distance;

    public EndPoint(String id, String name, String direction, Double distance) {
        super(id, name);
        this.distance = distance;
        this.direction = direction;
    }
}
