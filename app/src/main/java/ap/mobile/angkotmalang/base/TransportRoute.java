package ap.mobile.angkotmalang.base;

import java.util.ArrayList;

public class TransportRoute {

    private String[] order;
    private ArrayList<PathPoint> path = new ArrayList<>();
    private int cost;
    private double distance;
    private int numSwitch;
    private int dijkstraTime;
    private int graphTime;

    public TransportRoute(int cost, double distance, int numSwitch, String[] order) {
        this.cost = cost;
        this.distance = distance;
        this.numSwitch = numSwitch;
        this.order = order;
    }

    public TransportRoute(int cost, double distance, int numSwitch, String[] order, ArrayList<PathPoint> path) {
        this(cost, distance, numSwitch, order);
        this.path = path;
    }

    public void addPath(PathPoint point) { this.path.add(point); }
    public void setPath(ArrayList<PathPoint> path) { this.path = path; }
    public ArrayList<PathPoint> getPath() { return this.path; }

    public void setTime(int graphTime, int dijkstraTime) {
        this.graphTime = graphTime;
        this.dijkstraTime = dijkstraTime;
    }

    public String[] getNameList() {
        ArrayList<String> nameList = new ArrayList<>();
        String lastName = "";
        String lastDirection = "";
        for (PathPoint p: path) {
            if(lastName.equals(p.name) && lastDirection.equals(p.direction)) continue;
            else {
                nameList.add(p.name + ":" + p.direction);
                lastName = p.name;
                lastDirection = p.direction;
            }
        }
        return nameList.toArray(new String[nameList.size()]);
    }

    public String[] getOrder() { return this.order; }

    public int getCost() { return this.cost; }
    public double getDistance() { return this.distance; }
    public int getGraphTime() { return this.graphTime; }
    public int getDijkstraTime() { return this.dijkstraTime; }

}
