package ap.mobile.angkotmalang;

public class Helper {

    public static double degreeToMeter(double degree) {
        return Math.round(degree / CDM.oneMeterInDegree);
    }
    public static double meterToDegree(double meter) {
        return meter * CDM.oneMeterInDegree;
    }

}
