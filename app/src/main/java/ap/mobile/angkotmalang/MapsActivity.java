package ap.mobile.angkotmalang;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Dash;
import com.google.android.gms.maps.model.Gap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PatternItem;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnSuccessListener;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import ap.mobile.angkotmalang.base.EndPoint;
import ap.mobile.angkotmalang.base.PathPoint;
import ap.mobile.angkotmalang.base.Point;
import ap.mobile.angkotmalang.base.TransportRoute;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleMap.OnMapLongClickListener,
        View.OnClickListener, RouteAdapter.RouteItemClickListener {

    private ActionBar mActionBar;
    private GoogleMap mMap;
    private FusedLocationProviderClient mFusedLocationClient;
    private String[] mPermissions = {
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
    };
    private int mLocationRequestCode = 77;
    private Marker mUserLocationMarker;
    private Marker mDestinationMarker;

    private ArrayList<Point> mStartingPoints = new ArrayList<>();
    private ArrayList<Point> mDestinationPoints = new ArrayList<>();

    private ArrayList<Polyline> mSelectedPathPolylines = new ArrayList<>();
    private ArrayList<TransportRoute> mRouteSet = new ArrayList<>();
    private RouteAdapter mRouteAdapter;
    private MaterialDialog mResultDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PreferenceManager.setDefaultValues(this, R.xml.pref_service, false);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        this.mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        this.mActionBar = this.getSupportActionBar();

        this.findViewById(R.id.fabNavigate).setOnClickListener(this);
        this.findViewById(R.id.fabDirection).setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.menu_settings:
                Intent settingsIntent = new Intent(this, SettingsActivity.class);
                this.startActivity(settingsIntent);
                break;
        }
        return true;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.mMap = googleMap;
        this.mMap.setOnMapLongClickListener(this);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, this.mPermissions, this.mLocationRequestCode);
            return;
        }
        showUserLocation();
    }

    private void showUserLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        this.mFusedLocationClient.getLastLocation()
            .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(Location location) {
                    // Got last known location. In some rare situations this can be null.
                    if (location != null) {
                        // Logic to handle location object
                        LatLng userLocation = new LatLng(location.getLatitude(), location.getLongitude());
                        mUserLocationMarker = mMap.addMarker(new MarkerOptions().position(userLocation));
                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(userLocation, 15f));
                    }
                }
            });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == this.mLocationRequestCode) {
            if(grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                this.showUserLocation();
            }
        }
    }

    @Override
    public void onMapLongClick(LatLng latLng) {
        if(this.mDestinationMarker != null) this.mDestinationMarker.remove();
        this.mDestinationMarker = this.mMap.addMarker(
                new MarkerOptions()
                        .position(latLng)
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE))
        );
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fabNavigate:

                if(this.mDestinationMarker == null) {
                    new MaterialDialog.Builder(this)
                            .title("Information")
                            .content("You need to mark your destination by long-clicking the desired location on map before you can navigate")
                            .positiveText("OK")
                            .show();
                    return;
                }


                final MaterialDialog loadingDialog = new MaterialDialog.Builder(this)
                        .title("Processing...")
                        .content("Getting Angkot nearby...")
                        .progress(true, 5)
                        .progressIndeterminateStyle(true)
                        .show();

                String distance = PreferenceManager.getDefaultSharedPreferences(this)
                        .getString("pref_distance", String.valueOf(CDM.defaultDistance));

                String url = CDM.baseUrl(this) + "service.points.json"
                        + "?lat=" + this.mUserLocationMarker.getPosition().latitude
                        + "&lng=" + this.mUserLocationMarker.getPosition().longitude
                        + "&d=" + distance;
                this.mStartingPoints.clear();
                JsonArrayRequest startingPointsJsonArrayRequest
                        = new JsonArrayRequest(url, new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            for (int i = 0; i < response.length(); i++) {
                                JSONObject pointJsonObject = response.getJSONObject(i);
                                EndPoint point = new EndPoint(
                                        pointJsonObject.getString("id"),
                                        pointJsonObject.getString("name"),
                                        pointJsonObject.getString("direction"),
                                        pointJsonObject.getDouble("distance")
                                );
                                mStartingPoints.add(point);
                            }
                        } catch (Exception ex) {}
                        Toast.makeText(MapsActivity.this, "Complete! " + mStartingPoints.size() + " points added as starting point.", Toast.LENGTH_SHORT).show();
                        requestMonitor(loadingDialog);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(MapsActivity.this, "Error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
                        requestMonitor(loadingDialog);
                    }
                });


                url = CDM.baseUrl(this) + "service.points.json"
                        + "?lat=" + this.mDestinationMarker.getPosition().latitude
                        + "&lng=" + this.mDestinationMarker.getPosition().longitude
                        + "&d=" + distance;
                this.mDestinationPoints.clear();
                JsonArrayRequest destinationPointsJsonArrayRequest
                        = new JsonArrayRequest(url, new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            for (int i = 0; i < response.length(); i++) {
                                JSONObject pointJsonObject = response.getJSONObject(i);
                                EndPoint point = new EndPoint(
                                        pointJsonObject.getString("id"),
                                        pointJsonObject.getString("name"),
                                        pointJsonObject.getString("direction"),
                                        pointJsonObject.getDouble("distance")
                                );
                                mDestinationPoints.add(point);
                            }
                        } catch (Exception ex) {}
                        Toast.makeText(MapsActivity.this, "Complete! " + mDestinationPoints.size() + " points added as destination point.", Toast.LENGTH_SHORT).show();
                        requestMonitor(loadingDialog);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(MapsActivity.this, "Error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
                        requestMonitor(loadingDialog);
                    }
                });

                this.mRequestToCheck = 2;
                RestService.getInstance(this)
                        .addToRequestQueue(startingPointsJsonArrayRequest);
                RestService.getInstance(this)
                        .addToRequestQueue(destinationPointsJsonArrayRequest);

                break;
            case R.id.fabDirection:

                this.mResultDialog = new MaterialDialog.Builder(this)
                        .title("Route List")
                        .adapter(this.mRouteAdapter, null)
                        .positiveText("OK")
                        .show();

                break;
        }
    }

    int mRequestToCheck = 0;
    private void requestMonitor(MaterialDialog dialogToDismiss) {
        this.mRequestToCheck--;
        if(this.mRequestToCheck == 0) {

            dialogToDismiss.dismiss();

            this.mRouteSet.clear();
            if(this.mRouteAdapter != null)
                this.mRouteAdapter.notifyDataSetChanged();

            if(this.mStartingPoints.size() == 0 || this.mDestinationPoints.size() == 0) {
                new MaterialDialog.Builder(this)
                        .title("Error")
                        .content("Unable to find stops nearby to starting location or destination location")
                        .positiveText("OK")
                        .show();
                return;
            }

            for(int s = 0; s<this.mStartingPoints.size(); s++) {
                for(int d = 0; d<this.mDestinationPoints.size(); d++) {
                    Point sPoint = this.mStartingPoints.get(s);
                    Point dPoint = this.mDestinationPoints.get(d);
                    findRoute(sPoint, dPoint);
                }
            }

            if(this.mRouteAdapter == null)
                this.mRouteAdapter = new RouteAdapter(this, this.mRouteSet, this);

            this.mResultDialog = new MaterialDialog.Builder(this)
                    .title("Route List")
                    .adapter(this.mRouteAdapter, null)
                    .positiveText("OK")
                    .show();

        }
    }

    private void findRoute(Point sPoint, Point dPoint) {
        String url = CDM.baseUrl(this) + "service.json"
                + "?s=" + sPoint.getId()
                + "&d=" + dPoint.getId()
                + "&p=c"; // TODO: store priority as preference

        JsonObjectRequest routeJsonObjectRequest = new JsonObjectRequest(url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        if(response == null) return;
                        //int cost, double distance, int numSwitch, String[] order
                        try {

                            JSONArray orderJsonArray = response.getJSONArray("order");
                            String[] orderArray = new String[orderJsonArray.length()];
                            for(int i = 0; i<orderJsonArray.length(); i++)
                                orderArray[i] = orderJsonArray.getString(i);

                            TransportRoute transportRoute = new TransportRoute(
                                    response.getInt("cost"),
                                    response.getDouble("distance"),
                                    response.getInt("switch"),
                                    orderArray
                            );

                            ArrayList<PathPoint> path = new ArrayList<>();
                            JSONArray pathJsonArray = response.getJSONArray("path");
                            for(int i = 0; i<pathJsonArray.length(); i++)
                            {
                                JSONObject pointJsonObject = pathJsonArray.getJSONObject(i);
                                // String id, String name, String direction,
                                // String color, Double lat, Double lng, Double distance, int price
                                PathPoint point = new PathPoint(
                                        pointJsonObject.getString("id"),
                                        pointJsonObject.getString("name"),
                                        pointJsonObject.getString("direction"),
                                        pointJsonObject.getString("color"),
                                        pointJsonObject.getDouble("lat"),
                                        pointJsonObject.getDouble("lng"),
                                        pointJsonObject.getDouble("distance"),
                                        pointJsonObject.getInt("price")
                                        );

                                path.add(point);
                            }

                            transportRoute.setPath(path);
                            mRouteSet.add(transportRoute);
                            mRouteAdapter.notifyDataSetChanged();
                            //displayPath();

                        } catch (Exception ex) { }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });

        RestService.getInstance(this).addToRequestQueue(routeJsonObjectRequest);
    }

    private void displayPath(ArrayList<PathPoint> path) {

        for (Polyline polyline: this.mSelectedPathPolylines) {
            polyline.remove();
        }
        this.mSelectedPathPolylines.clear();

        PolylineOptions polylineOptions = new PolylineOptions();
        polylineOptions.width(7f);
        ArrayList<LatLng> points = new ArrayList<>();
        String prevColor = "";

        for (PathPoint point: path) {

            if(prevColor.equals("")) {
                polylineOptions = new PolylineOptions()
                        .width(9f)
                        .color(Color.parseColor(point.getColor()));
                points.add(point.getLatLng());
                prevColor = point.getColor();
            } else {

                if(!point.getColor().equals(prevColor)) {

                    polylineOptions.addAll(points);
                    Polyline pathPolyline = mMap.addPolyline(polylineOptions);
                    this.mSelectedPathPolylines.add(pathPolyline);
                    points.clear();

                    polylineOptions = new PolylineOptions()
                            .width(9f)
                            .color(Color.parseColor(point.getColor()));
                    points.add(point.getLatLng());
                    prevColor = point.getColor();

                } else {

                    points.add(point.getLatLng());

                }
            }



            /*
            points.add(point.getLatLng());

            if(color == point.getColor()) continue;
            else {
                if(!color.equals("")) {
                    polylineOptions.addAll(points);
                    Polyline pathPolyline = mMap.addPolyline(polylineOptions);
                    this.mSelectedPathPolylines.add(pathPolyline);
                    points.clear();
                }
                color = point.getColor();
                polylineOptions.color(Color.parseColor(color));
            }
            */
        }

        polylineOptions.addAll(points);
        Polyline pathPolyline = mMap.addPolyline(polylineOptions);
        this.mSelectedPathPolylines.add(pathPolyline);
        points.clear();

        Point startPoint = path.get(0);
        Point endPoint = path.get(path.size()-1);

        polylineOptions = new PolylineOptions()
                .width(9f)
                .color(Color.parseColor("#FFC107"));
        points.add(this.mUserLocationMarker.getPosition());
        points.add(startPoint.getLatLng());
        polylineOptions.addAll(points);

        List<PatternItem> pattern = Arrays.asList(
                new Gap(10), new Dash(30));

        Polyline walkingStartPolyline = mMap.addPolyline(polylineOptions);
        walkingStartPolyline.setPattern(pattern);

        points.clear();
        polylineOptions = new PolylineOptions()
                .width(9f)
                .color(Color.parseColor("#FFC107"));
        points.add(this.mDestinationMarker.getPosition());
        points.add(endPoint.getLatLng());
        polylineOptions.addAll(points);

        Polyline walkingEndPolyline = mMap.addPolyline(polylineOptions);
        walkingEndPolyline.setPattern(pattern);

        this.mSelectedPathPolylines.add(walkingStartPolyline);
        this.mSelectedPathPolylines.add(walkingEndPolyline);

    }

    @Override
    public void onRouteItemClicked(TransportRoute transportRoute) {
        displayPath(transportRoute.getPath());
        if(this.mResultDialog != null && this.mResultDialog.isShowing())
            this.mResultDialog.dismiss();
    }
}
